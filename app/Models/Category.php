<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Advertisement;
use Illuminate\Support\Facades\Storage;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'image', 'slug'];

    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        return Storage::disk('public')->url($this->image);
    }

    public function subcategories()
    {
        return $this->hasMany(Subcategory::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function ads()
    {
        return $this->hasMany(Advertisement::class);
    }

    //scope
    public function scopeCategoryCar($query)
    {
        return $query->where('name', 'vehicles')->first();
    }

    public function scopeCategoryElectronic($query)
    {
        return $query->where('name', 'Electronics')->first();
    }














    //public function getNameAttribute($value){
    // return strtoupper($value);
    // }

}
